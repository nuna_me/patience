FROM python:3.12-slim-bullseye
LABEL authors="Nuna"

# environment variables
ENV PIP_DISABLE_PIP_VERSION_CHECK 1
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONBUFFERED 1

# work directory
WORKDIR /app

# install requirements
COPY ./requirements.txt .
RUN pip install -r requirements.txt

# copy project
COPY . .
