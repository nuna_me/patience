"""
django application configuration of the consultation application
"""
from django.apps import AppConfig


class ConsultationConfig(AppConfig):
    """
    consultation configuration class
    """

    default_auto_field = "django.db.models.BigAutoField"
    name = "apps.consultation"
