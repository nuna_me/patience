"""
defines all models of consultation application
"""
from django.db import models
from django.db.models import ForeignKey


class Consultation(models.Model):
    class ConsultationType(models.TextChoices):
        VISITE = "Visite", "Visite classique"
        SUIVI = "Suivi", "Suivi d'un traitement ou d'une pathologie"
        OPERATION = "Opération", "Opération médicale"

    name = models.CharField(
        default="consultation classique",
        max_length=50,
        help_text="Intitulé de la consultation",
    )
    description = models.TextField(
        max_length=300,
        help_text="Décrivez le motif de la consultation",
        blank=True,
    )
    creation_date = models.DateTimeField(auto_now_add=True)

    consultation_type = models.CharField(choices=ConsultationType)

    # relationships
    patient = ForeignKey(
        "users.CustomBaseUser", on_delete=models.CASCADE, related_name="patient"
    )
    doctor = ForeignKey(
        to="users.CustomBaseUser", on_delete=models.CASCADE, related_name="doctor"
    )

    def __str__(self):
        return self.name
