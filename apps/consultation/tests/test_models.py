"""
test module that defines all tests of the consultation application (WIP)
"""
import datetime

from django.core.exceptions import ValidationError
from django.test import TestCase

from apps.consultation.models import Consultation
from django.contrib.auth import get_user_model


class ConsultationModelTest(TestCase):
    def setUp(self):
        user_model = get_user_model()
        self.doctor = user_model.objects.create(
            date_of_birth=datetime.date(1995, 2, 2),
            username="the_john_doe",
            first_name="john",
            last_name="doe",
            email="john@doe.com",
            address="14, rue du Bachaga Boualam 93420 Villepinte",
            is_staff=True,
            is_superuser=True,
        )
        self.patient = user_model.objects.create(
            date_of_birth=datetime.date(1995, 2, 2),
            username="the_jane_doe",
            first_name="jane",
            last_name="doe",
            email="jane@doe.com",
            address="14, rue du Bachaga Boualam 93420 Villepinte",
        )

    def test_type_consultation(self):
        try:
            Consultation.objects.create(
                patient=self.patient,
                doctor=self.doctor,
                description="La patiente s'est présentée aujourd'hui avec des maux de gorge",
                consultation_type=("Check-up", "Check-up annuel"),
            )
        except ValidationError as e:
            print("erreur")
