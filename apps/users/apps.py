"""
django application configuration of the users application
"""
from django.apps import AppConfig


class UsersConfig(AppConfig):
    """
    users configuration class
    """

    default_auto_field = "django.db.models.BigAutoField"
    name = "apps.users"
