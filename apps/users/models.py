"""
defines all models of users application
"""
# pylint: disable=too-many-arguments
import datetime

from django.contrib.auth.validators import UnicodeUsernameValidator
from django.db import models
from django.contrib.auth.models import AbstractUser, BaseUserManager

from apps.users.validators import FrenchAddressValidator


class CustomBaseUserManager(BaseUserManager):
    """
    Custom user manager which creates 3 types of users
    Patient users, Doctor users and Supervisor users
    """

    def _create_user(
        self,
        username,
        email,
        date_of_birth,
        address=None,
        password=None,
        **extra_fields
    ):
        """
        Default method to create a new user.

        Doctors and supervisors will be initialized by other methods that will eventually
        call this one.
        :param username: string
        :param email: string
        :param date_of_birth: datetime.date
        :param address: string (validated along a peculiar regular expression)
        :param password: string (validated along a regular expression)
        :return: a User object
        """
        # raise error if username is not provided
        if not username:
            raise ValueError("Ce champ est requis")

        # test date_of_birth
        if not date_of_birth:
            raise ValueError("Ce champ est requis")

        # user creation
        user = self.model(
            username=username,
            email=self.normalize_email(email),
            date_of_birth=date_of_birth,
            address=address,
            **extra_fields
        )

        # setting user password
        if not password:
            # in case of doctor create a patient
            user.set_password(self.make_random_password())
        else:
            # in other cases
            user.set_password(password)

        # commit all modifications
        user.save(using=self._db)
        return user

    def create_patient(
        self,
        username,
        email,
        date_of_birth,
        address=None,
        password=None,
        **extra_fields
    ):
        """
        Patients are regular users without any administrator permission.
        :param username:
        :param email:
        :param date_of_birth:
        :param address:
        :param password:
        :param extra_fields:
        :return:
        """
        extra_fields.setdefault("is_staff", False)
        extra_fields.setdefault("is_superuser", False)

        return self._create_user(
            username, email, date_of_birth, address, password, **extra_fields
        )

    def create_doctor(
        self,
        username,
        email,
        date_of_birth,
        address=None,
        password=None,
        **extra_fields
    ):
        """
        Administrators are the doctors (they have all privileges)
        :param username: string
        :param email: string
        :param date_of_birth: datetime.date
        :param address: string (validated along a peculiar regular expression)
        :param password: string (validated along a regular expression)
        :return: a User object
        """
        extra_fields.setdefault("is_staff", True)
        extra_fields.setdefault("is_superuser", True)

        return self._create_user(
            username, email, date_of_birth, address, password, **extra_fields
        )


class CustomBaseUser(AbstractUser):
    """
    Class that defines users in the application.
    Doctors are users with superuser privileges while Patients are regular users.
    Patients can only view their consultations, their doctors and their profile.
    """

    # Specifying to use our custom user manager
    objects = CustomBaseUserManager()

    # Specifying required fields
    REQUIRED_FIELDS = ["date_of_birth"]

    username = models.CharField(
        verbose_name="Nom d'utilisateur",
        help_text="Requis. Nécessite maximum 150 caractères. Seuls sont acceptés les lettres, "
        "les chiffres et les caractères suivants : @/./+/-/_.",
        max_length=150,
        unique=True,
        validators=[UnicodeUsernameValidator()],
        error_messages={
            "unique": "Un autre utilisateur possède déjà ce nom.",
        },
    )

    first_name = models.CharField(verbose_name="prénom", max_length=150, blank=True)

    last_name = models.CharField(
        verbose_name="nom de famille", max_length=150, blank=True
    )

    date_of_birth = models.DateField(verbose_name="Date de naissance")

    address = models.CharField(
        null=True,
        verbose_name="adresse",
        max_length=228,  # 6 lines of 38 characters
        help_text="Entrez votre adresse postale. Exemple : '1 rue de la paix 75001 Paris'",
        validators=[FrenchAddressValidator],
    )

    is_staff = models.BooleanField(
        default=False,
        verbose_name="équipier ?",
        help_text="Est-ce que l'utilisateur fait partie de l'équipe médicale ?",
    )

    is_superuser = models.BooleanField(
        default=False,
        verbose_name="médecin ?",
        help_text="Est-ce que l'utilisateur est un médecin (administrateur) ?",
    )

    class Meta:
        constraints = [
            # maintain data integrity for
            # username rules in database ...
            models.CheckConstraint(
                check=models.Q(username__regex=r"^[\w.@+-]{9,150}\Z"),
                name="username_regex",
            ),
            # ... french addresses rules ...
            models.CheckConstraint(
                check=models.Q(
                    address__regex=r"\d+,([ ]([a-zA-Z])+)+ \d{5} ([a-zA-Z])+"
                ),
                name="address_regex",
            ),
            # ... and minimum age to access the application
            models.CheckConstraint(
                check=models.Q(
                    date_of_birth__lte=datetime.date(
                        datetime.date.today().year,
                        datetime.date.today().month,
                        datetime.date.today().day,
                    )
                )
                | models.Q(is_staff=False, is_superuser=False),
                name="birth_year_lte_18",
            ),
        ]


# EVOLUTION : create a profile for each user
