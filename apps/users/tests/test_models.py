"""
test module that is currently used to test the "python manage.py test" command
"""
import datetime
import requests

from django.db import IntegrityError
from django.test import TestCase
from django.contrib.auth import get_user_model

from apps.utils.address_normalizer import normalize_json_addresses, python_to_url_query


class GeneralUserTest(TestCase):
    """
    Test cases common for all application users
    """

    def setUp(self):
        self.user_model = get_user_model()

    def test_invalid_username(self):
        """
        tests if a Doctor's username check all constraints
        :return:
        """
        with self.assertRaises(IntegrityError):
            user = self.user_model.objects._create_user(
                date_of_birth=datetime.date(1996, 2, 2),  # to test
                username="r^^''|",
                password="24012023DJA_",
                first_name="john",
                last_name="doe",
                email="john@doe.com",
                address="14, rue du Bachaga Boualam 93420 Villepinte",
            )

    def test_too_short_username(self):
        with self.assertRaises(IntegrityError) as err:
            self.user_model.objects._create_user(
                date_of_birth=datetime.date(1996, 2, 2),  # to test
                username="johndoe",
                password="24012023DJA_",
                first_name="john",
                last_name="doe",
                email="john@doe",
                address="14, rue du Bachaga Boualam 93420 Villepinte",
            )
        self.assertIn("username_regex", err.exception.args[0])

    def test_address_without_street_number(self):
        """
        :return:
        """
        with self.assertRaises(IntegrityError) as err:
            self.user_model.objects._create_user(
                date_of_birth=datetime.date(1996, 2, 2),  # to test
                username="john.doe1",
                password="24012023DJA_",
                first_name="john",
                last_name="doe",
                email="john@doe.com",
                address="rue du Bachaga Boualam 93420 Villepinte",
            )
        self.assertIn("address_regex", err.exception.args[0])

    def test_address_without_zipcode(self):
        """
        :return:
        """
        with self.assertRaises(IntegrityError) as err:
            self.user_model.objects._create_user(
                date_of_birth=datetime.date(1996, 2, 2),  # to test
                username="john.doe1",
                password="24012023DJA_",
                first_name="john",
                last_name="doe",
                email="john@doe.com",
                address="14, rue du Bachaga Boualam Villepinte",
            )
        self.assertIn("address_regex", err.exception.args[0])

    def test_address_with_big_street_number(self):
        with self.assertRaises(IntegrityError) as err:
            self.user_model.objects._create_user(
                date_of_birth=datetime.date(1996, 2, 2),  # to test
                username="john.doe2",
                password="24012023DJA_",
                first_name="john",
                last_name="doe",
                email="john@doe.com",
                address="144444 rue du Bachaga Boualam 93444 Villepinte",
            )
            self.assertIn("address_regex", err.exception.args[0])

    def test_api_addresses_without_street_number(self):
        # make a http request to api-adresse.data.gouv.fr
        # no street number specified, must fail
        response = requests.get(
            "https://api-adresse.data.gouv.fr/search/?q=rue+de+la+paix+paris"
        )

        # if not connected to the internet, pass the test
        if response.status_code != 200:
            print(response.status_code)
            return

        address_list = normalize_json_addresses(response)

        with self.assertRaises(IntegrityError):
            for index, address in enumerate(address_list):
                user = self.user_model.objects._create_user(
                    date_of_birth=datetime.date(1996, 2, 2),  # to test
                    username="john_doe" + str(index),
                    password="24012023DJA_",
                    first_name="john",
                    last_name="doe",
                    email="john@doe.com",
                    address=address,
                )
                self.assertIsNotNone(user)


class PatientTest(TestCase):
    """
    Specific tests for patient User
    """

    def setUp(self):
        self.user_model = get_user_model()

    def test_create_patient(self):
        patient = self.user_model.objects.create_patient(
            date_of_birth=datetime.date(2010, 2, 2),  # to test
            username="the_jane_doe",
            password="24012023DJA_",
            first_name="jane",
            last_name="doe",
            email="jane@doe.com",
            address="14, rue du Bachaga Boualam 93420 Villepinte",
        )
        # check role
        self.assertFalse(patient.is_staff)
        self.assertFalse(patient.is_superuser)

        self.assertEqual(patient.date_of_birth, datetime.date(2010, 2, 2))
        self.assertEqual(patient.username, "the_jane_doe")
        self.assertEqual(patient.first_name, "jane")
        self.assertEqual(patient.last_name, "doe")
        self.assertEqual(patient.email, "jane@doe.com")
        self.assertEqual(patient.address, "14, rue du Bachaga Boualam 93420 Villepinte")

        url = "https://api-adresse.data.gouv.fr/search/?q=" + python_to_url_query(
            patient.address
        )
        response = requests.get(url)

        address_list = normalize_json_addresses(response)

        self.assertEqual(1, len(address_list))

    def test_patient_permissions(self):
        pass


class DoctorTest(TestCase):
    """
    Specific tests for doctor User
    """

    def setUp(self):
        self.user_model = get_user_model()

    def test_create_doctor(self):
        doctor = self.user_model.objects.create_doctor(
            date_of_birth=datetime.date(1996, 2, 2),  # to test
            username="the_john_doe",
            password="24012023DJA_",
            first_name="john",
            last_name="doe",
            email="john@doe.com",
            address="25, avenue du Parc National 64260 Arudy",
        )
        self.assertTrue(doctor.is_staff)
        self.assertTrue(doctor.is_superuser)

        # check attributes
        self.assertEqual(doctor.date_of_birth, datetime.date(1996, 2, 2))
        self.assertEqual(doctor.username, "the_john_doe")
        self.assertEqual(doctor.first_name, "john")
        self.assertEqual(doctor.last_name, "doe")
        self.assertEqual(doctor.email, "john@doe.com")
        self.assertEqual(doctor.address, "25, avenue du Parc National 64260 Arudy")

        url = "https://api-adresse.data.gouv.fr/search/?q=" + python_to_url_query(
            doctor.address
        )
        response = requests.get(url)

        address_list = normalize_json_addresses(response)

        self.assertEqual(1, len(address_list))

    def test_invalid_date_of_birth(self):
        """
        tests if a user can be created if they are at least 18 years old from today
        :return:
        """
        # if the date_of_birth is later than 18 years ago
        with self.assertRaises(IntegrityError):
            doctor = self.user_model.objects.create_doctor(
                date_of_birth=datetime.date.today(),  # to test
                username="johndoe1",
                password="24012023DJA_",
                first_name="john",
                last_name="doe",
                email="john1@doe.com",
                address="14, rue du Bachaga Boualam 93420 Villepinte",
            )
            self.assertTrue(
                doctor.date_of_birth
                <= datetime.date.today() - datetime.timedelta(days=365.25) * 18,
                f"{doctor.date_of_birth} is not <= {datetime.date.today() - datetime.timedelta(days=365.25) * 18}",
            )

    def test_equal_date_of_birth(self):
        """
        must be true if date_of_birth equals today - 18 years
        :return:
        """
        today = datetime.date.today()
        exact_birth = datetime.date(today.year - 18, today.month, today.day)

        self.user_model.objects.create_doctor(
            date_of_birth=exact_birth,  # to test
            username="the_john_doe",
            password="24012023DJA_",
            first_name="john",
            last_name="doe",
            email="john2@doe.com",
            address="14, rue du Bachaga Boualam 93420 Villepinte",
        )

    def test_check_doctor_permission(self):
        doctor = self.user_model.objects.create_doctor(
            date_of_birth=datetime.date(1996, 2, 2),  # to test
            username="the_john_doe",
            password="24012023DJA_",
            first_name="john",
            last_name="doe",
            email="john@doe.com",
            address="14, rue du Bachaga Boualam 93420 Villepinte",
        )

        self.assertIsNotNone(doctor.get_all_permissions())
