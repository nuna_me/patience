import datetime

from django.core import validators


def legal_age_validator(date_of_birth, is_staff, is_superuser):
    today = datetime.date.today()
    limit_time = datetime.timedelta(days=365.25) * 18

    if is_staff and is_superuser:
        if date_of_birth < today - limit_time:
            raise validators.ValidationError(
                message="Un médecin doit avoir minimum 18 ans pour utiliser l'application",
                code="NOT_LEGAL_AGE",
            )


class FrenchAddressValidator(validators.RegexValidator):
    regex = r"\d+,([ ]([a-zA-Z])+)+ \d{5} ([a-zA-Z])+"
    message = "L'adresse n'est pas au bon format"
    code = "INVALID_FRENCH_ADDRESS"
