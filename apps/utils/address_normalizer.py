"""
utility functions to normalize json addresses
"""


def normalize_address(address):
    address = address.split(" ", 1)
    address = address[0] + ", " + address[1]
    return address


def normalize_json_addresses(json_addresses):
    address_list = []

    for raw_address in json_addresses.json()["features"]:
        address = normalize_address(raw_address["properties"]["label"])
        address_list.append(address)

    return address_list


def python_to_url_query(address):
    """
    convert address (string) to url query for the address api
    :param address:
    :return:
    """
    url_query = address.replace(",", "", 1)
    url_query = url_query.replace(" ", "+")
    return url_query
