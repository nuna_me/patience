"""
settings file to be used while ci pipelines launch tests
"""
import os

# pylint: disable=wildcard-import, unused-wildcard-import
from patience.settings.base import *

SECRET_KEY = os.environ.get("SECRET_KEY")

DEBUG = os.environ.get("DEBUG")

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql",
        "NAME": os.environ.get("POSTGRES_DB"),
        "USER": os.environ.get("POSTGRES_USER"),
        "PASSWORD": os.environ.get("POSTGRES_PASSWORD"),
        "HOST": "db",  # defined in gitlab-ci.yml
        "PORT": "5432",
        "TEST": {
            "NAME": os.environ.get("POSTGRES_DB") + "_test",
        },
    }
}

INSTALLED_APPS += ["django.contrib.staticfiles"]
