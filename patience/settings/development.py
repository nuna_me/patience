"""
settings file to be used while development by developers
"""
import os

# pylint: disable=wildcard-import, unused-wildcard-import
from patience.settings.base import *

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/5.0/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = env("SECRET_KEY")

DEBUG = True

# Database
# https://docs.djangoproject.com/en/5.0/ref/settings/#databases
# pylint: disable=duplicate-code
DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql",
        "NAME": env("POSTGRES_DB"),
        "USER": env("POSTGRES_USER"),
        "PASSWORD": env("POSTGRES_PASSWORD"),
        "PORT": env("POSTGRES_PORT"),
        "HOST": env("POSTGRES_HOST"),
        "TEST": {
            "NAME": "patience_test",
        },
    }
}

INSTALLED_APPS += ["django.contrib.staticfiles"]
