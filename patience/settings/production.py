"""
settings file to be used in production environment (WIP)
"""
# pylint: disable=wildcard-import, unused-wildcard-import
from patience.settings.base import *

SECRET_KEY = env("SECRET_KEY")

DEBUG = False

# Database
# https://docs.djangoproject.com/en/5.0/ref/settings/#databases
DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql",
        "NAME": env("PG_NAME"),
        "USER": env("PG_USER"),
        "PASSWORD": env("PG_PASSWD"),
        "PORT": env("PG_PORT"),
        "HOST": env("PG_HOST"),
    }
}
