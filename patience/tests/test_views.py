"""
    tests general views like error views
"""
from django.test import TestCase, tag
from django.urls import reverse

from patience.views import HomeView


@tag("core")
class HomeViewTest(TestCase):
    """
    HomeViewTest class that test
    - all view return status and content
    """

    def test_status_and_content_home_view(self):
        """
        test if home_view returns 200 (OK)
        """
        response = self.client.get(reverse("home"))
        self.assertContains(
            response,
            "Bienvenue sur Patience",
        )
        self.assertIs(response.resolver_match.func.view_class, HomeView)

    def test_home_view_template(self):
        """
        HomeView must use base and extending home templates
        """
        response = self.client.get(reverse("home"))
        self.assertTemplateUsed(response, "patience/home.html")
        self.assertTemplateUsed(response, "base.html")


@tag("core")
class View404ErrorTest(TestCase):
    """
    Tests 404 error view and template
    """

    def test_status_404_view(self):
        """
        get a 404 error when trying to reach a non existant resource
        """
        response = self.client.get("404/")
        self.assertEqual(response.status_code, 404)


@tag("core")
class View500ErrorTest(TestCase):
    """
    Tests 500 error view and template
    """

    def test_status_500_view(self):
        """
        get a 500 error when the server get an error while calculating
        """


@tag("core")
class View403ErrorTest(TestCase):
    """
    Tests 403 error view and template
    """

    def test_status_403_view(self):
        """
        return a 403 error while non-staff user tries to get hospital staff pages
        return a 403 error while non-authenticate user tries to get a patient page
        return a 403 error while authenticated users try to get another patient page
        """


@tag("core")
class View400ErrorTest(TestCase):
    """
    Tests 400 error view and template
    """

    def test_status_400_view(self):
        """
        return a 400 error while user tries to request with error in it
        example : malformed request syntax, invalid request message framing
        """
