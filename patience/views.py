"""
view module for the default page of the application (could be initialized in the urls module of
patience project)
"""
from django.views.generic import TemplateView


class HomeView(TemplateView):
    """
    return the homepage
    """

    template_name = "patience/home.html"
