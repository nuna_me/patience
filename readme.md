# PATIENCE

## Description

Patience is a web application for medical use. It helps hospital staff and doctors to manage patients 
and keep track on their health. Patients can retrieve their health data once their profile is created 
by the staff.

## Installation
The project is based on Python 3.12 and uses django 5.0, bootstrap 5 and postgresql.

Please, when cloning the project, install all dependencies listed in the requirements.txt file located
at the root of the project.

## Usage
As doctors or hospital staff : create a staff account. Next time you will have an appointment with
a patient, after asking for consent, add a new patient in the system with their first and last name,
email (mandatory for patient profile) and any other information they give.
On your dashboard, you will find list of consultations and list of patients you currently have.

As patient : you can create a password and a username in order to access your health data once you 
receive your first profile link. Next time you'll want to consult your profile, you'll have to 
authenticate with your username and password.

## Credit

## License

## Badges

## Tests
